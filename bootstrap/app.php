<?php
/**
 * @package PHPKit.
 * @author: mawenpei
 * @date: 2016/6/2
 * @time: 16:26
 */

require "autoload.php";

$app = new HuoCore\Application(realpath(APP_DIR));

$app->setNamespace('HuoService');


$app->singleton(
    Illuminate\Contracts\Http\Kernel::class,
    HuoCore\Http\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    HuoCore\Console\Kernel::class
);

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    HuoCore\Exceptions\Handler::class
);

return $app;