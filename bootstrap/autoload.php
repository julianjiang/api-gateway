<?php

!defined('LARAVEL_START') && define('LARAVEL_START', microtime(true));

//定义PHP通用框架根目录
!defined('PHPFW_DIR') && define('PHPFW_DIR',dirname(dirname(__FILE__)));
//定义应用根目录
!defined('APP_DIR') && define('APP_DIR',dirname(dirname(__FILE__)));

require  PHPFW_DIR . '/vendor/autoload.php';

Illuminate\Support\ClassLoader::addDirectories([
    APP_DIR . '/services/'
]);


Illuminate\Support\ClassLoader::register();