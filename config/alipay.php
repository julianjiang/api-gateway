<?php
/**
 * 支付宝开放平台配置信息
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/24
 * Time: 12:02
 */
return [
//支付宝开发平台appid
    "appId"=>"", 
    //私钥
    "rsaPrivateKey"=>"",
    //支付宝公钥
    "alipayrsaPublicKey"=>"",
];