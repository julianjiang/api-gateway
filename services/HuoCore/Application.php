<?php
/**
 * @package PHPKit.
 * @author: mawenpei
 * @date: 2016/8/1
 * @time: 9:40
 */
namespace HuoCore;

class Application extends \Illuminate\Foundation\Application
{
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
    }

    public function path()
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'services'.DIRECTORY_SEPARATOR . 'HuoService';
    }
}