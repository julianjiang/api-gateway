<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/21
 * Time: 11:08
 */

namespace HuoCore\Http;

use Illuminate\Http\Response;


/**
 * Class BaseHttpService
 * 对外提供的server基类，主要包括三个输出方法
 * @package HuoCore\Http
 */
abstract class BaseHttpService
{

    /**
     * 接口统一输出，成功返回
     * @param $data
     * @return $this
     */
    public static function outSuccess($data)
    {
        if(!isset($data['errorCode'])){
            $data = ['errorCode'=>0,'result'=>$data,'errorDescription'=>''];
        }
        echo json_encode($data,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        return;
//        return (new Response(json_encode($data,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES)))->header('Content-Type','text/json');
    }

    /**
     * 接口统一输出，错误返回
     * @param int $errorCode
     * @param $errorDescription
     * @param $result
     * @return $this
     */
    public static function outError($errorCode=300, $errorDescription,$result=[])
    {
        $out = ['errorCode'=>$errorCode,'result'=>$result,'errorDescription'=>$errorDescription];
        echo json_encode($out,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        return;
//        return (new Response(json_encode($out,JSON_UNESCAPED_UNICODE)))->header('Content-Type','text/json');
    }


    /** 接口统一输出，原样返回
     * @param $data
     * @return $this
     */
    public static function outJson($data)
    {
        echo json_encode($data,JSON_UNESCAPED_UNICODE|JSON_UNESCAPED_SLASHES);
        return;
//        return (new Response(json_encode($data,JSON_UNESCAPED_UNICODE)))->header('Content-Type','text/json');
    }
}