<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/21
 * Time: 14:13
 */

namespace HuoCore\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    protected $middleware = [];
    protected $routeMiddleware = [
        'auth'=>'HuoCore\\Http\\Middleware\\EncryptRSA'
    ];
}