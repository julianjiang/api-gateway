<?php

namespace HuoCore\Http\Middleware;

use Closure;
use HuoCore\Helper\Utility;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;

class EncryptRSA
{
    /**
     * The names of the cookies that should not be encrypted.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    public function handle($request, Closure $next)
    {
//        if(!Config::get("app.debug")){
            $result = Utility::encrypt($request);
            if(!$result['status']){
                $out = ['errorCode'=>403,'result'=>'','errorDescription'=>$result['msg']];
                return (new Response(json_encode($out,JSON_UNESCAPED_UNICODE)))->header('Content-Type','text/json');
            }
//        }

        return $next($request);
    }
}
