<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/21
 * Time: 11:06
 */

namespace HuoCore\Logic;


/**
 * Class BaseLogic
 * logic基类，两个对哇i输出方法
 * @package HuoCore\Logic
 */
abstract class BaseLogic
{
    public static function outSuccess($result,$appends=[])
    {
        $out = ['errorCode'=>0,'result'=>$result,'errorDescription'=>''];
        if(isset($appends['result'])) unset($appends['result']);
        if(isset($appends['errorDescription'])) unset($appends['errorDescription']);
        $out = $appends ? array_merge($out,$appends) : $out;
        return $out;
    }
    public static function outError($errorDescription,$errorCode=300,$result=[])
    {
        $out = ['errorCode'=>$errorCode,'result'=>$result,'errorDescription'=>$errorDescription];
        return $out;
    }

    /** 统一输出，原样返回
     * @param $data
     * @return
     */
    public static function outData($data)
    {
        return $data;
    }
}