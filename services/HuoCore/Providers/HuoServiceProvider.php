<?php
/**
 * @package PHPKit.
 * @author: mawenpei
 * @date: 2016/5/18
 * @time: 15:26
 */
namespace HuoCore\Providers;

use Illuminate\Support\ServiceProvider;

class HuoServiceProvider extends ServiceProvider
{
    public function boot()
    {

    }

    public function register()
    {

    }
    protected function registerCommands()
    {

    }

    public function provides()
    {

    }
}