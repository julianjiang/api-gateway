<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/21
 * Time: 11:13
 */

namespace HuoCore\Request;

use Hprose\Future;
use Hprose\Http\Client;
use Exception;
/**
 * Class BaseRequest
 * RPC http客户端基类
 * @package HuoCore\Request
 */
abstract class BaseRpcRequest
{
    protected $serverUrl;

    private $client;

    /**
     *  construct , new a Hprose http client
     */
    public function __construct()
    {
        if(empty($this->serverUrl)){
            throw new Exception("未定义服务端链接");
        }
        $this->client = new Client($this->serverUrl,false);
    }


    /**
     * RPC CLIENT发送请求
     * @param string $function
     * @param array $args
     * @throws Exception
     * @return mixed $value
     */
    public function sendRequest($function='', $args=array()){
        if(empty($function)){
            throw new Exception("未定义远程服务");
        }
        try{
            $response = $this->client->$function($args);
            return $this->dealResponse($response);
        }catch (Exception $e){
            throw new Exception("未连接到远程服务");
        }
    }


    /**
     * 处理rpc调用之后的结果，并返回
     * @param Future $response
     * @return Future
     */
    private function dealResponse($response){
        return $response;
    }

}