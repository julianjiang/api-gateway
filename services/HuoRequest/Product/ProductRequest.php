<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/21
 * Time: 13:56
 */

namespace HuoRequest\Product;

use HuoCore\Request\BaseRpcRequest;


/** 调用商品微服务的客户端
 * Class ProductRequest
 * @package HuoRequest\Product
 */
class ProductRequest extends BaseRpcRequest
{

    /**
     * Rpc服务的地址链接
     * @var string
     */
    protected $serverUrl = "http://192.168.10.6/a/Server.php";

    public function get($id){
        $result = $this->sendRequest("getProduct",array("id"=>$id));
        return $result;
    }
}