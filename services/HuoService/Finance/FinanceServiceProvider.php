<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/21
 * Time: 14:02
 */

namespace HuoService\Finance;

use Illuminate\Support\ServiceProvider;

class FinanceServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app['router']->group(['namespace'=>'HuoService','middleware'=>'auth'],function($router){
            require __DIR__ . '/routes.php';
        });
    }

    public function register()
    {

    }

    public function provides()
    {

    }
}