<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/25
 * Time: 9:30
 */

namespace HuoService\Finance\Logic;

use HuoCore\Logic\BaseLogic;
use HuoService\Finance\Model\AlipayTransferRecordModel;
use Illuminate\Support\Facades\Config;

class AlipayLogic extends BaseLogic
{
    /** 支付宝转账
     * @param array $paydata
     * @return mixed
     */
    public static function transfer($paydata=array()){
      /**
       * paydata格式:
       * out_biz_no
       * payee_type
       * payee_account
       * amount
       * payer_show_name
       * payee_real_name
       * remark
      */
        require_once dirname(__FILE__)."/alipay/AopSdk.php";
        $config = Config::get('alipay');
        $aop = new \AopClient ();
        $aop->appId = $config['appId'];
        $aop->rsaPrivateKey = $config['rsaPrivateKey'];
        $aop->alipayrsaPublicKey = $config['alipayrsaPublicKey'];
        $aop->signType = 'RSA2';
        $request = new \AlipayFundTransToaccountTransferRequest ();
        $request->setBizContent(json_encode($paydata));
        $result = $aop->execute ( $request);

        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if(!empty($resultCode)&&$resultCode == 10000){
//            echo "成功";
//            成功则保存数据记录
            $paydata['order_id'] = $result->$responseNode->order_id;
            $alipayTransferRecordModel = new AlipayTransferRecordModel();
            $alipayTransferRecordModel->create($paydata);
            return self::outSuccess($paydata);
        } else {
//            echo "失败";
            return self::outError($result->$responseNode->sub_msg);
        }
    }
}