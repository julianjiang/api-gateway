<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/24
 * Time: 15:09
 */

namespace HuoService\Finance\Logic;


use HuoCore\Logic\BaseLogic;
use HuoService\Finance\Model\BillModel;

class BillLogic extends BaseLogic
{
    //    财务类型
    const MONEY_CZJL = 1;    //充值记录
    const MONEY_CGFK = 2;    //采购付款
    const MONEY_HB = 3;    //红包
    const MONEY_HTCZ = 4;    //后台充值
    const MONEY_ZZ = 5;    //转账
    const MONEY_TX = 6;    //提现
    const MONEY_QTKK = 7;    //其他扣款
    const MONEY_CGTK = 8;    //采购退款
    const MONEY_CJFH = 9;    //差价返还
    const MONEY_SHTK = 10;    //售后退款
    const MONEY_DFYF = 11;    //代发邮费
    const MONEY_DFJS = 12;    //代发结算
    const MONEY_FWFTH = 13;    //服务费退还
    const MONEY_YFTH = 14;    //邮费退还
    const MONEY_FWF = 15;    //服务费
    const MONEY_QTTK = 16;    //其他退款
    const MONEY_PSCZ = 17;    //配送充值

    static $moneyTypes = array(
        BillLogic::MONEY_CGFK   =>  array(
            "name"  =>  "采购付款",
            'icon'  =>  "templates/default/images/money_icon/2.jpg",
            'type'  =>  'minus',
        ),
        BillLogic::MONEY_DFYF   =>  array(
            "name"  =>  "代发邮费",
            'icon'  =>  "templates/default/images/money_icon/11.jpg",
            'type'  =>  'minus',
        ),
        BillLogic::MONEY_FWF   =>  array(
            "name"  =>  "服务费",
            'icon'  =>  "templates/default/images/money_icon/15.jpg",
            'type'  =>  'minus',
        ),
        BillLogic::MONEY_QTKK   =>  array(
            "name"  =>  "其他扣款",
            'icon'  =>  "templates/default/images/money_icon/7.jpg",
            'type'  =>  'minus',
        ),
        BillLogic::MONEY_CZJL   =>  array(
            "name"  =>  "充值记录",
            'icon'  =>  "templates/default/images/money_icon/1.jpg",
            'type'  =>  'plus',
        ),
        BillLogic::MONEY_HB   =>  array(
            "name"  =>  "红包",
            'icon'  =>  "templates/default/images/money_icon/3.jpg",
            'type'  =>  'plus',
        ),
        BillLogic::MONEY_CJFH   =>  array(
            "name"  =>  "差价返还",
            'icon'  =>  "templates/default/images/money_icon/9.jpg",
            'type'  =>  'plus',
        ),
        BillLogic::MONEY_CGTK   =>  array(
            "name"  =>  "采购退款",
            'icon'  =>  "templates/default/images/money_icon/8.jpg",
            'type'  =>  'plus',
        ),
        BillLogic::MONEY_FWFTH   =>  array(
            "name"  =>  "服务费退还",
            'icon'  =>  "templates/default/images/money_icon/13.jpg",
            'type'  =>  'plus',
        ),
        BillLogic::MONEY_YFTH   =>  array(
            "name"  =>  "邮费退还",
            'icon'  =>  "templates/default/images/money_icon/14.jpg",
            'type'  =>  'plus',
        ),
        BillLogic::MONEY_DFJS   =>  array(
            "name"  =>  "代发结算",
            'icon'  =>  "templates/default/images/money_icon/12.jpg",
            'type'  =>  'plus',
        ),
        BillLogic::MONEY_SHTK   =>  array(
            "name"  =>  "售后退款",
            'icon'  =>  "templates/default/images/money_icon/10.jpg",
            'type'  =>  'plus',
        ),
        BillLogic::MONEY_HTCZ   =>  array(
            "name"  =>  "后台充值",
            'icon'  =>  "templates/default/images/money_icon/4.jpg",
            'type'  =>  'plus',
        ),
        BillLogic::MONEY_ZZ   =>  array(
            "name"  =>  "转账",
            'icon'  =>  "templates/default/images/money_icon/5.jpg",
            'type'  =>  'minus'
        ),
        BillLogic::MONEY_TX   =>  array(
            "name"  =>  "提现",
            'icon'  =>  "templates/default/images/money_icon/6.jpg",
            'type'  =>  'minus'
        ),
        BillLogic::MONEY_PSCZ   =>  array(
            "name"  =>  "配送续费",
            'icon'  =>  "templates/default/images/money_icon/17.jpg",
            'type'  =>  'minus'
        ),
        BillLogic::MONEY_QTTK   =>  array(
            "name"  =>  "其他退款",
            'icon'  =>  "templates/default/images/money_icon/16.jpg",
            'type'  =>  'plus'
        ),
    );

    //  获取收入或支出的所有账务类型
    public static function getMoneyTypes($type = 'plus'){
        $moneyTypes = self::moneyTypes;
        $types = array();
        foreach ($moneyTypes as $key => $moneyType) {
            if($moneyType['type'] == $type){
                $types[$key] = $moneyType;
            }
        }
        return $types;
    }

    public static function addBillOut($datas){
        $datas['type'] = "minus";
        if (isset($datas['name']))
        {
            $datas['class'] = 'usr';
        }
        else
        {
            $datas['name'] = "出账";
            $datas['class'] = 'sys';
        }
        if (isset($datas['intro']))
        {
            $datas['class'] = 'usr';
        }
        else
        {
            $datas['intro'] = '出账';
            $datas['class'] = 'sys';
        }

//        用户余额操作
        $moneyOperateResult = UserLogic::updateUserMoney($datas['userid'],$datas['money'],0);
        if($moneyOperateResult['errorCode']){
            return self::outError($moneyOperateResult['errorDescription']);
        }
        return self::addBill($datas);
    }
    public static function addBillIn($datas){
        $datas['type'] = "plus";
        if (isset($datas['name']))
        {
            $datas['class'] = 'usr';
        }
        else
        {
            $datas['name'] = "进账";
            $datas['class'] = 'sys';
        }
        if (isset($datas['intro']))
        {
            $datas['class'] = 'usr';
        }
        else
        {
            $datas['intro'] = '进账';
            $data['class'] = 'sys';
        }
//        用户余额操作
        $moneyOperateResult = UserLogic::updateUserMoney($datas['userid'],$datas['money'],1);
        if($moneyOperateResult['errorCode']){
            return self::outError($moneyOperateResult['errorDescription']);
        }
        return self::addBill($datas);
    }

    private static function addBill($datas){
        $current_money = UserLogic::userMoney($datas['userid']);
        $data = array(
            'userid' => array_get($datas, 'userid'),
            'type' => array_get($datas, 'type','plus'),
            'money' => array_get($datas, 'money'),
            'time' => time(),
            'money_type'  =>   array_get($datas, 'money_type'),
            'deal_flow'  =>	array_get($datas, 'deal_flow',''),
            'current_money'=>(float)$current_money,
            'zq_money' => (float)array_get($datas, 'zq_money','0'),
            'coupon_value'=>(float)array_get($datas, 'coupon_value','0'),
        );
        if (isset($datas['name']))
        {
            $data['name'] = $datas['name'];
            $data['class'] = 'usr';
        }
        else
        {
            $data['name'] = "进账";
            $data['class'] = 'sys';
        }
        if (isset($datas['intro']))
        {
            $data['intro'] = $datas['intro'];
            $data['class'] = 'usr';
        }
        else
        {
            $data['intro'] = '进账';
            $data['class'] = 'sys';
        }
        $billModel = new BillModel();
        $result =  $billModel->insertGetId($data);
        if($result){
            return self::outSuccess($result);
        }else{
            return self::outError("生成账单失败");
        }
    }
}