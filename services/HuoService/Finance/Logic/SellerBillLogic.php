<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/25
 * Time: 15:09
 */

namespace HuoService\Finance\Logic;

use HuoCore\Logic\BaseLogic;
use HuoService\Finance\Model\SellerBillModel;

class SellerBillLogic extends BaseLogic
{
    //    财务类型
    const MONEY_DD = 1;    //订单
    const MONEY_TX = 2;    //提现
    const MONEY_KAKD = 5;    //ka扣点
    const MONEY_KF = 6;    //后台扣款
    const MONEY_CJKD = 10;    //差价扣点
    const MONEY_GMZW = 11;    //购买展位广告
    const MONEY_CZ = 12;    //充值
    const MONEY_BT = 100;    //使用白条


    static $moneyTypes = array(
        SellerBillLogic::MONEY_DD   =>  array(
            "name"  =>  "订单结算",
            'icon'  =>  "",
            'type'  =>  'plus',
        ),
        SellerBillLogic::MONEY_TX   =>  array(
            "name"  =>  "提现",
            'icon'  =>  "",
            'type'  =>  'minus',
        ),
        SellerBillLogic::MONEY_KAKD   =>  array(
            "name"  =>  "ka扣点",
            'icon'  =>  "",
            'type'  =>  'minus',
        ),
        SellerBillLogic::MONEY_CJKD   =>  array(
            "name"  =>  "差价扣点",
            'icon'  =>  "",
            'type'  =>  'minus',
        ),
        SellerBillLogic::MONEY_GMZW   =>  array(
            "name"  =>  "购买展位",
            'icon'  =>  "",
            'type'  =>  'minus',
        ),
        SellerBillLogic::MONEY_CZ   =>  array(
            "name"  =>  "充值",
            'icon'  =>  "",
            'type'  =>  'plus',
        ),
        SellerBillLogic::MONEY_BT   =>  array(
            "name"  =>  "白条",
            'icon'  =>  "",
            'type'  =>  'minus',
        ),
    );

    //  获取收入或支出的所有账务类型
    public static function getMoneyTypes($type = 'plus'){
        $moneyTypes = self::moneyTypes;
        $types = array();
        foreach ($moneyTypes as $key => $moneyType) {
            if($moneyType['type'] == $type){
                $types[$key] = $moneyType;
            }
        }
        return $types;
    }

    public static function addBillOut($datas){
        $datas['type'] = "minus";
        if (isset($datas['name']))
        {
            $datas['class'] = 'usr';
        }
        else
        {
            $datas['name'] = "出账";
            $datas['class'] = 'sys';
        }
        if (isset($datas['intro']))
        {
            $datas['class'] = 'usr';
        }
        else
        {
            $datas['intro'] = '出账';
            $datas['class'] = 'sys';
        }

//        用户余额操作
        $moneyOperateResult = SellerLogic::updateSellerMoney($datas['sellerid'],$datas['money'],0);
        if($moneyOperateResult['errorCode']){
            return self::outError($moneyOperateResult['errorDescription']);
        }
        return self::addBill($datas);
    }
    public static function addBillIn($datas){
        $datas['type'] = "plus";
        if (isset($datas['name']))
        {
            $datas['class'] = 'usr';
        }
        else
        {
            $datas['name'] = "进账";
            $datas['class'] = 'sys';
        }
        if (isset($datas['intro']))
        {
            $datas['class'] = 'usr';
        }
        else
        {
            $datas['intro'] = '进账';
            $data['class'] = 'sys';
        }
//        用户余额操作
        $moneyOperateResult = SellerLogic::updateSellerMoney($datas['sellerid'],$datas['money'],1);
        if($moneyOperateResult['errorCode']){
            return self::outError($moneyOperateResult['errorDescription']);
        }
        return self::addBill($datas);
    }

    private static function addBill($datas){
        $current_money = SellerLogic::sellerMoney($datas['sellerid']);
        $data = array(
            'sellerid' => array_get($datas, 'sellerid'),
            'type' => array_get($datas, 'type','plus'),
            'money' => array_get($datas, 'money'),
            'time' => time(),
            'money_type'  =>   array_get($datas, 'money_type'),
            'deal_flow'  =>	array_get($datas, 'deal_flow',''),
            'current_money'=>(float)$current_money,
            'zq_money' => (float)array_get($datas, 'zq_money','0'),
            'coupon_value'=>(float)array_get($datas, 'coupon_value','0'),
        );
        if (isset($datas['name']))
        {
            $data['name'] = $datas['name'];
            $data['class'] = 'usr';
        }
        else
        {
            $data['name'] = "进账";
            $data['class'] = 'sys';
        }
        if (isset($datas['intro']))
        {
            $data['intro'] = $datas['intro'];
            $data['class'] = 'usr';
        }
        else
        {
            $data['intro'] = '进账';
            $data['class'] = 'sys';
        }
        $billModel = new SellerBillModel();
        $result =  $billModel->insertGetId($data);
        if($result){
            return self::outSuccess($result);
        }else{
            return self::outError("生成账单失败");
        }
    }
}