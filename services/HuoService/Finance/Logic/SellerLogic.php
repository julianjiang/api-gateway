<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/25
 * Time: 14:30
 */

namespace HuoService\Finance\Logic;

use HuoCore\Logic\BaseLogic;
use HuoService\Finance\Model\SellerModel;

class SellerLogic extends BaseLogic
{
    /**
     * 根据用户id获取某个档口的信息
     * @param $userid
     * @return array
     */
    public static function uSeller($userid){
        $seller = SellerModel::where("userid",$userid)->first();
        if($seller){
            return self::outData($seller->toArray());
        }else{
            return self::outError("档口不存在");
        }
    }

    /**
     * 根据id获取某个档口的信息
     * @param $id
     * @return array
     */
    public static function seller($id){
        $seller = SellerModel::where("id",$id)->first();
        if($seller){
            return self::outData($seller->toArray());
        }else{
            return self::outError("档口不存在");
        }
    }


    /**
     * 获取档口余额
     * @param $id
     * @return mixed
     */
    public static function sellerMoney($id){
        $seller = SellerModel::where("id",$id)->first()->toArray();
        return self::outData((float)$seller['money']);
    }


    /**
     * 更新余额
     * @param $id
     * @param $money
     * @param int $type ,1增加，0减少
     */
    public static function updateSellerMoney($id, $money, $type = 1){
//        $currentMoney = self::sellerMoney($id);
//        if($type == 0 && $currentMoney < $money){
//            self::outError("余额不够，扣款失败");
//        }
        if($type){
            $updateMoneyResult = SellerModel::where("id",$id)->increment("money",$money);
        }else{
            $updateMoneyResult = SellerModel::where("id",$id)->decrement("money",$money);
        }
        if(!$updateMoneyResult){
            self::outError("系统发送错误，扣款失败");
        }
        self::outSuccess($updateMoneyResult);
    }
}