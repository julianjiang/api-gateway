<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/24
 * Time: 15:29
 */

namespace HuoService\Finance\Logic;

use HuoCore\Logic\BaseLogic;
use HuoService\Finance\Model\UserModel;

class UserLogic extends BaseLogic
{

    /**
     * 获取某个用户的信息
     * @param $userid
     * @return array
     */
    public static function user($userid){
        $user = UserModel::where("uid",$userid)->first();
        if($user){
            return self::outData($user->toArray());
        }else{
            return self::outError("账户不存在");
        }
    }
    /**
     * 获取账户余额
     * @param $userid
     * @return mixed
     */
    public static function userMoney($userid){
        $user = UserModel::where("uid",$userid)->first()->toArray();
        return self::outData((float)$user['money']);
    }


    /**
     * 更新余额
     * @param $userid
     * @param $money
     * @param int $type ,1增加，0减少
     */
    public static function updateUserMoney($userid, $money, $type = 1){
//        $currentMoney = self::userMoney($userid);
//        if($type == 0 && $currentMoney < $money){
//            self::outError("余额不够，扣款失败");
//        }
        if($type){
            $updateMoneyResult = UserModel::where("uid",$userid)->increment("money",$money);
        }else{
            $updateMoneyResult = UserModel::where("uid",$userid)->decrement("money",$money);
        }
        if(!$updateMoneyResult){
            self::outError("系统发送错误，扣款失败");
        }
        self::outSuccess($updateMoneyResult);
    }
}