<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/24
 * Time: 11:56
 */

namespace HuoService\Finance\Model;

use Illuminate\Database\Eloquent\Model;

class AlipayTransferRecordModel extends Model
{
    protected $connection = 'mysql_finance';
    protected $table = "finance_alipay_transfer_record";
    protected $fillable = ['out_biz_no','payee_type','payee_account','amount','payer_show_name','payee_real_name','remark','order_id'];
}