<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/24
 * Time: 15:04
 */

namespace HuoService\Finance\Model;

use Illuminate\Database\Eloquent\Model;
class BillModel extends Model
{
    protected $connection = 'mysql_hx';
    protected $table = "tttuangou_usermoney";
    public $timestamps = false;
}