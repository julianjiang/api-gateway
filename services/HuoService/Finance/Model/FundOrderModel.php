<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/24
 * Time: 11:50
 */

namespace HuoService\Finance\Model;

use Illuminate\Database\Eloquent\Model;

class FundOrderModel extends Model
{
    protected $connection = 'mysql_finance';
    protected $table = "finance_fund_order";
    protected $fillable = ['userid','sellerid','amount','paytype','paydata','fee','receive_account','orderid'];
}