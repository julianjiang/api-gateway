<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/24
 * Time: 15:04
 */

namespace HuoService\Finance\Model;

use Illuminate\Database\Eloquent\Model;
class SellerBillModel extends Model
{
    protected $connection = 'mysql_hx';
    protected $table = "tttuangou_seller_hezuo_money";
    public $timestamps = false;
}