<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/25
 * Time: 14:29
 */

namespace HuoService\Finance\Model;

use Illuminate\Database\Eloquent\Model;
class SellerModel extends Model
{
    public $timestamps = false;
    protected $connection = 'mysql_hx';
    protected $table = "tttuangou_seller";
}