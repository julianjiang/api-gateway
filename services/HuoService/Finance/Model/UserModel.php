<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/24
 * Time: 11:58
 */

namespace HuoService\Finance\Model;

use Illuminate\Database\Eloquent\Model;
class UserModel extends Model
{
    public $timestamps = false;
    protected $connection = 'mysql_hx';
    protected $table = "system_members";
}