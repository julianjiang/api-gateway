<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/21
 * Time: 14:26
 */

namespace HuoService\Finance\Proxy;

use HuoCore\Http\BaseHttpService;
use HuoService\Finance\Logic\FundLogic;
use Illuminate\Support\Facades\Request;


/**
 * 提现相关接口
 * Class FundHttpService
 * @package HuoService\Finance\Proxy
 */
class FundHttpService extends BaseHttpService
{

    /**
     * 支付宝提现接口
     * @return $this
     */
    public static function alipay(){
        $data = [
            "payee_account" =>  Request::input("payee_account"),
            "amount" =>  Request::input("amount"),
            "userid" =>  Request::input("userid"),
            "payee_real_name" =>  Request::input("payee_real_name"),
            "paytype"   =>  1,
        ];
        $result = FundLogic::create($data);
        return self::outJson($result);
    }


    public static function pay(){
        $orderid = Request::input("orderid");
        $result = FundLogic::pay($orderid);
        return self::outJson($result);
    }

}