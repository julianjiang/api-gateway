<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/21
 * Time: 16:01
 */

namespace HuoService\Product\Logic;


use HuoCore\Logic\BaseLogic;
use HuoRequest\Product\ProductRequest;

class ProductLogic extends BaseLogic
{
    public static function get($id){
        $productClient = new ProductRequest();
        $result = $productClient->get($id);
        return $result;
    }
}