<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/21
 * Time: 14:02
 */

namespace HuoService\Product;

use Illuminate\Support\ServiceProvider;

class ProductServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app['router']->group(['namespace'=>'HuoService','middleware'=>'auth'],function($router){
            require __DIR__ . '/routes.php';
        });
    }

    public function register()
    {

    }

    public function provides()
    {

    }
}