<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/21
 * Time: 14:26
 */

namespace HuoService\Product\Proxy;

use HuoCore\Http\BaseHttpService;
use HuoService\Product\Logic\ProductLogic;


/**
 * 商品的接口，例如增删改查等
 * Class ProductHttpService
 * @package HuoService\Product\Proxy
 */
class ProductHttpService extends BaseHttpService
{
    public static function create(){

    }

    public static function get(){
        $id = 34;
        $product = ProductLogic::get($id);
        return self::outSuccess($product);
    }
}