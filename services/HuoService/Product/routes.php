<?php
/**
 * Created by PhpStorm.
 * User: jiangjun
 * Date: 2017/4/21
 * Time: 14:03
 */
Route::any('/product-create',function(){
    return HuoService\Product\Proxy\ProductHttpService::create();
});
Route::get('/product-get',function(){
    return HuoService\Product\Proxy\ProductHttpService::get();
});